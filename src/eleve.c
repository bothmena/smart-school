#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "eleve.h"
#include "util.h"

int main() {

	modifier( 1 );
    printFile();
	return 0;
}

Eleve getEleveById( int id ) {

	FILE *f;
	Eleve eleve;
	Eleve eleveResult;
    eleveResult.id = 0;

	f=fopen("BDD/eleve.txt","r");
    if(f !=NULL) {

        while( fscanf( f, "%d %s %s %d %s\n", &(eleve.id), eleve.nom, eleve.prenom, &(eleve.age), eleve.classe ) != EOF){
            if ( id == eleve.id ) {
                printf("%d - %s %s\n", eleve.id, eleve.nom, eleve.prenom );
                eleveResult = eleve;
            }
        }
        fclose(f);
    }
    return eleveResult;
}

void creer() {

	Eleve eleve;

	eleve.id = getEntityId();
	printf("Creation eleve...\n");
	inputStr( "nom: ", eleve.nom, 50 );	
	inputStr( "prenom: ", eleve.prenom, 50 );
	printf("age: ");
	scanf("%d", &(eleve.age) );
	getchar();
	inputStr( "classe: ", eleve.classe, 10 );

	insertInDB( eleve );	
}

void modifier( int id ) {

    Eleve eleve = getEleveById( id );
    if ( eleve.id == 0 ) {
        printf( "eleve with id '%d' was not found in the DB!\n", id );
        return;
    }
	char aux_str[50];
	int aux_int;
	printf("-- modification eleve --\n");

	inputStr( "nom: ", aux_str, 50 );
	if ( *aux_str != '\0' )
		strcpy( eleve.nom, aux_str );

	inputStr( "prenom: ", aux_str, 50 );
        if ( *aux_str != '\0' )
                strcpy( eleve.prenom, aux_str );

	printf( "age: " );
        scanf( "%d", &aux_int );
        getchar();
	if( aux_int > 0 )
		eleve.age = aux_int;

	inputStr( "classe: ", aux_str, 10 );
        if ( *aux_str != '\0' )
                strcpy( eleve.classe, aux_str );

    FILE *f;
    Eleve eleveDB;
    f=fopen("BDD/eleve-tmp.txt","a+");
    if(f!=NULL) {

        while( fscanf( f, "%d %s %s %d %s\n", &(eleveDB.id), eleveDB.nom, eleveDB.prenom, &(eleveDB.age), eleveDB.classe ) != EOF){
            if ( eleveDB.id == eleve.id )
                insertInDB( eleve );
            else
                insertInDB( eleveDB );
        }
        replaceFile( "BDD/eleve-tmp.txt", "BDD/eleve.txt" );
    } else
        printf("Impossible d'ouvrir le fichier");
}

void supprimer( int id ) {

    FILE *f;
    Eleve eleveDB;
    f=fopen("BDD/eleve-tmp.txt","a+");
    if(f!=NULL) {

        while( fscanf( f, "%d %s %s %d %s\n", &(eleveDB.id), eleveDB.nom, eleveDB.prenom, &(eleveDB.age), eleveDB.classe ) != EOF){
            if ( eleveDB.id == id )
                continue;
            else
                insertInDB( eleveDB );
        }
        replaceFile( "BDD/eleve-tmp.txt", "BDD/eleve.txt" );
    } else
        printf("Impossible d'ouvrir le fichier");
}

void afficher( Eleve *elevep ) {

        printf("eleve = {\n\tid: %d;\n\tnom: %s;\n\tprenom: %s;\n\tage: %d;\n\tclasse: %s;\n};\n"
                , (*elevep).id, (*elevep).nom, (*elevep).prenom, (*elevep).age, (*elevep).classe );
}

void inputStr( char *msg, char *input, int length ) {

	printf( "%s", msg );
	fgets( input, length, stdin );

	size_t ln = strlen(input) - 1;
	if ( input[ln] == '\n' )
		input[ln] = '\0';
}

void insertInDB( Eleve eleve ) {
	
	FILE *f;
	f=fopen("BDD/eleve.txt","a+");
    spaceToUS( eleve.nom );
    spaceToUS( eleve.prenom );
    spaceToUS( eleve.classe );
	if(f!=NULL) {
		fprintf( f, "%d %s %s %d %s\n", eleve.id, eleve.nom, eleve.prenom, eleve.age, eleve.classe );
		fclose(f);
	} else
		printf("Impossible d'ouvrir le fichier");
}

void printFile() {

    FILE *f;
    Eleve eleve;
    f=fopen("BDD/eleve.txt","r");
    if( f != NULL ) {

        while( fscanf( f, "%d %s %s %d %s\n", &(eleve.id), eleve.nom, eleve.prenom, &(eleve.age), eleve.classe ) != EOF ){

            uSToSpace( eleve.nom );
            uSToSpace( eleve.prenom );
            uSToSpace( eleve.classe );
            afficher( &eleve );
            printf("------------------------------------\n");
        }
        fclose(f);
    }
}

int getEntityId() {

    FILE *f;
    int lastId = 0;
    Eleve eleve;

    f=fopen( "BDD/eleve.txt", "r" );
    if( f != NULL ) {

        while( fscanf( f, "%d %s %s %d %s\n", &(eleve.id), eleve.nom, eleve.prenom, &(eleve.age), eleve.classe ) != EOF){
            lastId = eleve.id;
        }
        fclose(f);
    }
    else {
        printf("the file 'eleve.txt' could not be opened!\n");
    }
    return ++lastId;
}
