//
// Created by bothmena on 14/11/16.
//

#ifndef SMART_SCHOOL_UTIL_H
#define SMART_SCHOOL_UTIL_H

char *spaceToUS( char *input );
char *uSToSpace( char *input );
void replaceFile( char *fileName, char *newFileName );

#endif //SMART_SCHOOL_UTIL_H
