#ifndef SMART_SCHOOL_ELEVE_
#define SMART_SCHOOL_ELEVE_

typedef struct EleveDef {
	int id;
	char nom[50];
	char prenom[50];
	int age;
	char classe[10];
} Eleve;

void creer();
Eleve getEleveById( int id );
void modifier( int id );
void afficher( Eleve *elevep );
void supprimer( int id );
void insertInDB( Eleve eleve );
void inputStr( char *msg, char *input, int length );
void printFile();
int getEntityId();

#endif

