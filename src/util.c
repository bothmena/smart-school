// Created by bothmena on 14/11/16.

# include <string.h>
# include <stdio.h>

void spaceToUS(char *input ) {

    int i = 0;
    size_t len = strlen( input );
    while ( i < len ) {

        if ( *input == ' ' )
            *input = '_';
        input++; i++;
    }
}

void uSToSpace( char *input ) {

    int i = 0;
    size_t len = strlen( input );
    while ( i < len ) {

        if ( *input == '_' )
            *input = ' ';
        input++; i++;
    }
}

void replaceFile( char *fileName, char *newFileName ) {

    int resRm = remove( newFileName );
    if( resRm == 0 ){

        int resRn = rename( fileName, newFileName );
        if(resRn != 0) {
            printf("Error: unable to rename the file\n");
            perror("Error");
        }
    }
    else {
        printf("Unable to delete the file\n");
        perror("Error");
    }
}
